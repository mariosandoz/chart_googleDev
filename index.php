<!DOCTYPE html>

<?php
$db = new mysqli('localhost', 'root', '', 'sandbox');

$res = $db->query("SELECT
                    month(d.devdate) as monthDev,sum(d.man) as man, sum(d.woman) as woman
                FROM 
                    `devotion_summary` d 
                group by 
                    monthDev
                "
);
if (!$res) {
    die($db->error);
}
$devSum[0] = array('Bulan', 'Laki-laki', 'Perempuan');
$i = 1;

while ($row = $res->fetch_assoc()) {
    $devSum[$i] = array($row['monthDev'], (int)$row['man'], (int)$row['woman']);
    $i++;
}
?>
<html>
    <head>
        <script type="text/javascript"
                src="https://www.google.com/jsapi?autoload={
                'modules':[{
                'name':'visualization',
                'version':'1',
                'packages':['corechart']
                }]
        }"></script>
        <script>
            var devSum = <?php echo json_encode($devSum)?>;
        </script>

        <script type="text/javascript">
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                    var data = google.visualization.arrayToDataTable(devSum);
                            var options = {
                            title: 'Devotion Summary',
                                    curveType: 'function',
                                    legend: { position: 'bottom' }
                            };
                            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                            chart.draw(data, options);
                    }
        </script>
    </head>
    <body>
        <div id="curve_chart" style="width: 900px; height: 500px"></div>
    </body>
</html>

