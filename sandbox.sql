-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 25, 2015 at 03:23 PM
-- Server version: 5.5.44-MariaDB-1ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sandbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `devotion_summary`
--

CREATE TABLE IF NOT EXISTS `devotion_summary` (
  `devotionid` int(11) NOT NULL AUTO_INCREMENT,
  `devdate` date NOT NULL,
  `sessionid` tinyint(4) NOT NULL,
  `man` int(7) NOT NULL,
  `woman` int(7) NOT NULL,
  PRIMARY KEY (`devotionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `devotion_summary`
--

INSERT INTO `devotion_summary` (`devotionid`, `devdate`, `sessionid`, `man`, `woman`) VALUES
(1, '2015-08-23', 1, 112, 215),
(2, '2015-08-23', 2, 215, 315),
(3, '2015-08-23', 3, 26, 37),
(4, '2015-08-23', 4, 89, 125),
(5, '2015-08-30', 1, 89, 125),
(6, '2015-08-30', 2, 185, 285),
(7, '2015-08-30', 3, 25, 65),
(8, '2015-08-30', 4, 85, 125),
(9, '2015-09-06', 1, 85, 125),
(10, '2015-09-06', 2, 285, 325);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `sessionid` tinyint(4) NOT NULL AUTO_INCREMENT,
  `waktu` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`sessionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`sessionid`, `waktu`) VALUES
(1, 'Kebaktian pukul 6'),
(2, 'Kebaktian pukul 9'),
(3, 'Kebaktian pukul 14'),
(4, 'Kebaktian pukul 18'),
(5, 'Kebaktian sekolah minggu');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
